

class Solution(object):

    def remove_duplicates(self, nums):
        """
        1.1 Remove Duplicates from Sorted Array
        原题
        给定一个有序的数组，删除重复元素并返回新数组长度。
        注意点：
        空间复杂度为O(1)
        例子：
        输入：A = [1,1,2] 输出：2 且 A = [1,2]
        解题思路
        采用两个指针i和count从左到右遍历，i记录的当前位置，count记录新数组的当前可插入位置。
        Args:
            nums:

        Returns:

        """
        if len(nums) == 0:
            return 0
        count = 0
        for num in nums:
            if nums[count] != num:
                count += 1
                nums[count] = num
        return count + 1


if __name__ == '__main__':
    s = Solution().remove_duplicates([1, 1, 2, 3])
    print(s)
