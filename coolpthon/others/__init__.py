import gc

import objgraph as objgraph


class A(object):
    def __init__(self):
        self.other = None

    def set_other(self, other):
        self.other = other


class B(object):
    def __init__(self, other):
        self.other = other


if __name__ == '__main__':
    gc.disable()
    for i in range(10000):
        a = A()
        b = B(a)
        a.set_other(b)
    print(objgraph.most_common_types(50))
